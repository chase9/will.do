# Will.Do Changelog

### Beta 4 - Build 99
- Added task searching to overview
- Today view shows overdue tasks and tasks from today
- Bug fixes
 - Fixed crash on new task list creation
 - Fixed color picker overlap
 - Fixed tasks not actually being deleted (imagine that!)
  - Tasks that were previously orphaned will now appear in a repaired task list.
- UI Updates
 - Fixed black tint when viewing task
 - Removed dark mode toggle (sorry)
- Performance enhancements
 - Today view only reloads what's been changed.
 - Overview doesn't always try to move task lists
- Added ability to 3D touch on tasks to preview them
- Added badge for overdue tasks
- Keyboard appears automatically when editing tasks.

### Beta 3 - Build 89
- Updated UI
 - Task list view has a new UI and color picker
 - Task labels use multiple lines with long titles
 - Tables stay looking nice when entering edit mode
- Fixed checkboxes
- Fixed notifications not working
- Fixed list headers turning white
- A new task list is created when deleting the last one

### Beta 2 - Build 79
- Editing a task will now display the proper navigation title.
- Worked on making the overall UI better
- Switched to using a Realm backend

### Beta 1 - Build 65
- Initial release to TestFlight
