# _Will.Do_

## Welcome
Will.Do is an open source To-Do/Task Manager by Chase Lau, which was conceived as a way for me to to learn Swift/Cocoa. Since there are so many task management apps already on the app store I figured I might as well just make it FOSS.

## Licence
Will.Do is licensed under the MIT license. If you'd like to read it, see [LICENSE](LICENSE).

## Credits
Here are frameworks I used in my project and would like to thank for making my life easier 😁
* [DateHelper](https://github.com/melvitax/DateHelper) for easy date manipulation
* [Chameleon](https://github.com/ViccAlexander/Chameleon) for a beautiful color scheme.
* [Realm](https://realm.io/) for their awesome data management system.
* [ChromaColorPicker](https://github.com/joncardasis/ChromaColorPicker) for a better way to select colors than I could've done.
