//
//  AppDelegate.swift
//  Will.Do
//
//  Created by Chase Lau on 1/1/18.
//  Copyright © 2018 Chase Lau. All rights reserved.
//

import UIKit
import UserNotifications
import os.log
import EventKit

// Workaround for Xcode 10 bug
#if swift(>=4.2)
    import UIKit.UIGeometry
    extension UIEdgeInsets {
        public static let zero = UIEdgeInsets()
    }
#endif

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var isGrantedNotificationAccess: Bool = false
    var remindersStatus: EKAuthorizationStatus = .notDetermined

    // MARK: - Standard AppDelegate
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        // Request authorization for notifications
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            // Enable or Disable features based on authorization.
            self.isGrantedNotificationAccess = granted
        }
        // Setup Notification categories
        let markCompleted = UNNotificationAction(
            identifier: "MARK_COMPLETED",
            title: "Mark Completed",
            options: []
        )
        let snooze15 = UNNotificationAction(
            identifier: "SNOOZE_15",
            title: "Snooze 15 Minutes",
            options: []
        )
        let taskCategory = UNNotificationCategory(
            identifier: "TASK",
            actions: [markCompleted, snooze15],
            intentIdentifiers: [],
            options: []
        )
        center.setNotificationCategories([taskCategory])
        center.delegate = self
        return true
    }

    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        if self.remindersStatus == .authorized {
            if shortcutItem.type == "me.chaselau.Will-Do.NewTask" {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let tabBar: UITabBarController = self.window?.rootViewController as! UITabBarController
                let overviewNavController: UINavigationController = tabBar.viewControllers?[0] as! UINavigationController
                let addNavController = storyboard.instantiateViewController(withIdentifier: "newTaskNavController")
                overviewNavController.present(addNavController, animated: true) {
                    completionHandler(true)
                }
            }
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        let predicate = TaskManager.shared.eventStore.predicateForIncompleteReminders(withDueDateStarting: nil, ending: Date(), calendars: nil)
        TaskManager.shared.eventStore.fetchReminders(matching: predicate) { (reminders) in
            DispatchQueue.main.async {
                UIApplication.shared.applicationIconBadgeNumber = reminders?.count ?? 0
            }
        }
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        self.remindersStatus = EKEventStore.authorizationStatus(for: .reminder)
        if self.remindersStatus != .authorized {
            os_log("Requesting reminders authorization", log: .default, type: .debug)
            let remindersView = UIStoryboard(name: "Extra", bundle: nil).instantiateViewController(withIdentifier: "reminders")
            self.window?.rootViewController?.present(remindersView, animated: true, completion: nil)
            TaskManager.shared.eventStore.requestAccess(to: .reminder) { (granted, error) in
                self.remindersStatus = EKEventStore.authorizationStatus(for: .reminder)
            }
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // MARK: - Notification Delegate

//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        let taskID = response.notification.request.identifier
//        if let task = TaskManager.shared.getTask(withID: taskID) {
//            switch response.actionIdentifier {
//            case "MARK_COMPLETED":
//                do {
//                    let realm = try Realm()
//                    try realm.write {
//                        task.completed = true
//                        completionHandler() }
//                } catch {
//                    os_log("Error maeking task as completed:\n%@", log: .default, type: .debug, error.localizedDescription)
//                }
//            case "SNOOZE_15":
//                let content = response.notification.request.content
//                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 900, repeats: false)
//                let request = UNNotificationRequest(identifier: taskID, content: content, trigger: trigger)
//
//                center.add(request) { error in
//                    if error != nil {
//                        fatalError("Unable to reschedule notification for 15 minutes.")
//                    } else {
//                        os_log("Notification snoozed for 15 minutes.", log: .default, type: .debug)
//                        completionHandler()
//                    }
//                }
//            default:
//                completionHandler()
//            }
//        } else {
//            fatalError("Unable to find task with matching ID.")
//        }
//    }
}

