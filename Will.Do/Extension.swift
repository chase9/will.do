//
//  Extensions.swift
//  Will.Do
//
//  Created by Chase Lau on 3/8/18.
//  Copyright © 2018 Chase Lau. All rights reserved.
//

import UIKit
import EventKit

// MARK: - Extends NSLayoutConstraints to be better identifiable when debugging.
extension NSLayoutConstraint {

    override open var description: String {
        let id = identifier ?? ""
        return "id: \(id), constant: \(constant)"
    }
}

// MARK: - Makes the cancel button of UISearchBar a selectable button.
// I use this to keep the cancel button enabled when dismissing firstResponder.
// From https://stackoverflow.com/a/43288405
extension UISearchBar {
    var cancelButton: UIButton? {
        for subView1 in subviews {
            for subView2 in subView1.subviews {
                if let cancelButton = subView2 as? UIButton {
                    return cancelButton
                }
            }
        }
        return nil
    }
}

extension EKReminder {
    var isOverdue: Bool {
        return (self.dueDateComponents?.date?.compare(Date()) == .orderedDescending)
    }
}
