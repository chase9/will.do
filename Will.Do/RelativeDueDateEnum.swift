//
//  RelativeDueDate.swift
//  Will.Do
//
//  Created by Chase Lau on 1/23/18.
//  Copyright © 2018 Chase Lau. All rights reserved.
//

import Foundation
enum RelativeDueDate: Int {
    // Remember to always keep someday as the final value in the enum
    case past = 0, lastWeek, yesterday, today, tomorrow, thisWeek, nextWeek, thisMonth, someday

    var string: String {
        switch self {
        case .past:
            return "Past"
        case .lastWeek:
            return "Last Week"
        case .yesterday:
            return "Yesterday"
        case .today:
            return "Today"
        case .tomorrow:
            return "Tomorrow"
        case .thisWeek:
            return "This Week"
        case .nextWeek:
            return "Next Week"
        case .thisMonth:
            return "This Month"
        case .someday:
            return "Someday"
        }
    }
}
