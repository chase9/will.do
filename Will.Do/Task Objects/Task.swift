////
////  Task.swift
////  Will.Do
////
////  Created by Chase Lau on 1/1/18.
////  Copyright © 2018 Chase Lau. All rights reserved.
////
//
//import UIKit
//import os.log
//import UserNotifications
//import DateHelper
//
//@objcMembers class Task: Object {
//
//    // MARK: Properties
//    
//    dynamic var id = NSUUID().uuidString
//    dynamic var title = ""
//    dynamic var dueDate = Date()
//    dynamic var note: String? = nil
//    dynamic var completed = false
//    var token: NotificationToken? = nil
//    
//    // One-to-one realm relationship (inverse)
//    let owningTaskList = LinkingObjects(fromType: TaskList.self, property: "tasks")
//    
//    override static func primaryKey() -> String {
//        return "id"
//    }
//    
//    override static func ignoredProperties() -> [String] {
//        return ["token"]
//    }
//    
//    var isOverdue: Bool {
//        if Date() >= dueDate {
//            return true
//        } else {
//            return false
//        }
//    }
//    var relativeDueDate: RelativeDueDate {
//        let now = Date()
//        let currentCalendar = Calendar.current
//
//        if currentCalendar.isDateInYesterday(dueDate) {
//            return .yesterday
//        } else if dueDate.compare(.isLastWeek) {
//            return .lastWeek
//        } else if dueDate < now.dateFor(.yesterday) {
//            return .past
//        } else if currentCalendar.isDateInToday(dueDate) {
//            return .today
//        } else if currentCalendar.isDateInTomorrow(dueDate) {
//            return .tomorrow
//        } else if dueDate.compare(.isThisWeek) {
//            return .thisWeek
//        } else if dueDate.compare(.isThisMonth) {
//            return .thisMonth
//        } else {
//            return .someday
//        }
//    }
//
//    // MARK: - Notifications
//    
//    func buildNotificationRequest() -> UNNotificationRequest {
//        let content = UNMutableNotificationContent()
//        content.title = self.title
//        content.body = self.note ?? "Due \(self.dueDate.toString(dateStyle: .short, timeStyle: .short))"
//        content.categoryIdentifier = "TASK"
//        content.sound = UNNotificationSound.default()
//
//        let calendar = Calendar.current
//        let dateComponents = DateComponents(
//            calendar: calendar,
//            year: calendar.component(.year, from: self.dueDate),
//            month: calendar.component(.month, from: self.dueDate),
//            day: calendar.component(.day, from: self.dueDate),
//            hour: calendar.component(.hour, from: self.dueDate),
//            minute: calendar.component(.minute, from: self.dueDate)
//        )
//        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
//
//        let request = UNNotificationRequest(identifier: self.id, content: content, trigger: trigger)
//
//        return request
//    }
//    
//    /// Removes self from the notification center
//    func removefromCenter() {
//        let center = UNUserNotificationCenter.current()
//        center.removeDeliveredNotifications(withIdentifiers: [self.id])
//        center.removePendingNotificationRequests(withIdentifiers: [self.id])
//        os_log("Removed from notification center", log: .default, type: .debug, self.title)
//    }
//
//    // MARK: - Public Functions
//    
//    func friendlyDate(withStyle style: DateFormatter.Style) -> String {
//        // Create DateFormatter instance
//        let dateFormatter = DateFormatter()
//        // Style for no time and full month displayed
//        dateFormatter.timeStyle = .none
//        dateFormatter.dateStyle = style
//        // Style from user's current locale
//        dateFormatter.locale = Locale.current
//        // Return formatted date
//        let formattedDate = dateFormatter.string(from: self.dueDate)
//        return formattedDate
//    }
//
//    static func == (lhs: Task, rhs: Task) -> Bool {
//        if lhs.id == rhs.id {
//            return true
//        } else {
//            return false
//        }
//    }
//    
//    deinit {
//        token?.invalidate()
//    }
//}
//
