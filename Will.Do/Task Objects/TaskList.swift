////
////  TaskList.swift
////  Will.Do
////
////  Created by Chase Lau on 1/23/18.
////  Copyright © 2018 Chase Lau. All rights reserved.
////
//
//import UIKit
//import os.log
//import Chameleon
//import UserNotifications
//
//@objcMembers class TaskList: Object {
//
//    // MARK: Properties
//
//    dynamic var id = NSUUID().uuidString
//    dynamic var title = ""
//    dynamic var listColor: String = UIColor.flatWhite().hexValue()
//
//    // Many-to-many Realm relationship
//    let tasks = List<Task>()
//
//    override static func primaryKey() -> String {
//        return "id"
//    }
//
//    // MARK: - Task Functions
//
//    func addTask(_ task: Task) {
//        do {
//            let realm = try Realm()
//            try realm.write {
//                self.tasks.append(task)
//            }
//        } catch {
//            os_log("Error adding task:\n%@", log: .default, type: .debug, error.localizedDescription)
//        }
//        // Add new task to NC
//        UNUserNotificationCenter.current().add(task.buildNotificationRequest(), withCompletionHandler: nil)
//        // Observe to handle notifications
//        task.token = task.observe({ change in
//            switch change {
//            case .deleted:
//                break;
//                // If task is changed, check to update the center
//            case .change(let properties):
//                for property in properties {
//                    // If completed switched to false, add to center, else remove it.
//                    if property.name == "completed" && property.newValue as! Bool == false {
//                        os_log("Adding task from NC via Realm:\n%@", log: .default, type: .info, task.title)
//                        UNUserNotificationCenter.current().add(task.buildNotificationRequest(), withCompletionHandler: nil)
//                    } else if property.name == "completed" && property.newValue as! Bool == true {
//                        os_log("Removing task from NC via Realm:\n%@", log: .default, type: .info, task.title)
//                        task.removefromCenter()
//                    }
//                }
//            case .error(let error):
//                fatalError(error.debugDescription)
//            }
//        })
//    }
//
//    func removeTask(_ task: Task) {
//        do {
//            let realm = try Realm()
//            try realm.write {
//                self.tasks.remove(at: self.tasks.index(of: task)!)
//            }
//        } catch {
//            os_log("Error removing task:\n%@", log: .default, type: .debug, error.localizedDescription)
//        }
//    }
//}
