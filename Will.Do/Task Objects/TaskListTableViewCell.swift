//
//  TasksListsTableViewCell.swift
//  Will.Do
//
//  Created by Chase Lau on 1/24/18.
//  Copyright © 2018 Chase Lau. All rights reserved.
//

import UIKit

class TaskListTableViewCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: Cell Overrides
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
