//
//  MasterTaskList.swift
//  Will.Do
//
//  MasterTaskList is a class designed to manage several TaskLists.
//  MasterTaskList is used to add/remove/move tasks between lists.
//
//  Created by Chase Lau on 1/26/18.
//  Copyright © 2018 Chase Lau. All rights reserved.
//

import UIKit
import os.log
import UserNotifications
import EventKit

final class TaskManager {

    // MARK: Singleton
    static let shared = TaskManager()

    // MARK: Stored Properties
    var eventStore: EKEventStore!
    var calendars: [EKCalendar]
    var reminders: [[EKReminder]]!

    var showCompletedTasks: Bool {
        get {
            return UserDefaults.standard.bool(forKey: #function)
        } set {
            UserDefaults.standard.set(newValue, forKey: #function)
        }
    }


    // Mark: Init
    init() {
        os_log("Initializing singleton", log: .default, type: .debug)
        self.eventStore = EKEventStore()
        self.calendars = [EKCalendar]()
        self.reminders = [[EKReminder]]()
        self.load()
        NotificationCenter.default.addObserver(self, selector: #selector(load), name: .EKEventStoreChanged, object: eventStore)
    }

/// Removes a task from a taskList by index
///
/// - Parameters:
///   - list: Index for the list to remove from
///   - at: Index for the task to remove
/// - Returns: The Task object removed
    @discardableResult func removeTask(fromList list: Int, at: Int) -> EKReminder? {
        if let deletedTask = self.getTask(inList: list, at: at) {
            do {
                self.reminders[list].remove(at: at)
                try eventStore.remove(deletedTask, commit: true)
                return deletedTask
            } catch {
                os_log("Unable to remove event from eventstore", log: .default, type: .error)
            }
        } else {
            os_log("Unable to find task", log: .default, type: .error)
        }
        return nil
    }


/// Returns a task at index from tasklist at index
///
/// - Parameters:
///   - list: Index for task list
///   - at: index for task
/// - Returns: A task if found, otherwise nil
    func getTask(inList list: Int, at: Int) -> EKReminder? {
        if list >= calendars.count || at >= reminders[list].count {
            return nil
        } else {
            return reminders[list][at]
        }
    }

/// Finds are returns the task with matching ID.
///
/// - Parameter id: The ID of the task to look for.
/// - Returns: A Task if found, otherwise nil.
//    func getTask(withID id: String) -> Task? {
//        for list in calendars {
//            for task in list.tasks {
//                if task.id == id {
//                    return task
//                }
//            }
//        }
//        return nil
//    }

    func updateTask(_ newTask: EKReminder) {
        do {
            try eventStore.save(newTask, commit: true)
        } catch {
            os_log("Unable to save new task", log: .default, type: .error)
        }
    }

// MARK: TaskList Functions
// Adds a TaskList to the masterList
    func addTaskList(_ taskList: EKCalendar) {
        do {
            try eventStore.saveCalendar(taskList, commit: true)
        } catch {
            os_log("Error adding task list:\n%@", log: .default, type: .debug, error.localizedDescription)
        }
    }

    // Removes a TaskList from the masterList
    func removeTaskList(at: Int) {
        // Remove the list from the masterList
        do {
            try eventStore.removeCalendar(calendars[at], commit: true)
        } catch {
            os_log("Error removing task list:\n%@", log: .default, type: .debug, error.localizedDescription)
        }
    }

/// Returns the TaskList at index, if exists
///
/// - Parameter at: Index of the TaskList
/// - Returns: A TaskList is one exists, nil otherwise
    func getTaskList(at: Int) -> EKCalendar? {
        if at >= calendars.count {
            return nil
        } else {
            return calendars[at]
        }
    }

    func getTaskListForTask(_ task: EKReminder) -> EKCalendar? {
        return task.calendar
    }

/// Attempts to find the index for the passed TaskList
///
/// - Parameter taskList: The TaskList to find index for
/// - Returns: Returns the index for the passed TaskList, or nil if list cannot be found.
    func getTaskListIndex(for taskList: EKCalendar) -> Int {
        if let foundList = calendars.index(of: taskList) {
            return foundList
        } else {
            return -1
        }
    }

//    func setTaskListTitle(to title: String, at: Int) {
//        do {
//            try realm.write {
//                masterList[at].title = title
//            }
//        } catch {
//            os_log("Error setting task list title:\n%@", log: .default, type: .debug, error.localizedDescription)
//        }
//    }
//
    func setTaskListColor(to color: CGColor, at: Int) {
        calendars[at].cgColor = color
    }

// MARK: Public Functions
    @objc func load() {
        os_log("Loading TaskManager data", log: .default, type: .debug)
        self.calendars = self.eventStore.calendars(for: .reminder)
        for i in 0..<self.calendars.count {
            let predicate = showCompletedTasks ? self.eventStore.predicateForReminders(in: [self.calendars[i]]) : self.eventStore.predicateForIncompleteReminders(withDueDateStarting: nil, ending: nil, calendars: [self.calendars[i]])
            self.eventStore.fetchReminders(matching: predicate, completion: { (reminders: [EKReminder]?) in
                if let reminders = reminders {
                    self.reminders.append(reminders)
                    NotificationCenter.default.post(name: Notification.Name("RemindersLoaded"), object: nil)
                }
            })
        }
        os_log("Done loading TaskManager data", log: .default, type: .debug)
    }

    func reloadEventStore() {
        os_log("Reloading event store", log: .default, type: .debug)
        self.eventStore = EKEventStore()
        self.calendars = [EKCalendar]()
        self.reminders = [[EKReminder]]()
        self.eventStore.requestAccess(to: .reminder) { (granted, error) in
            if granted {
                self.load()
            }
        }
    }

    func count() -> Int {
        return calendars.count
    }
}

