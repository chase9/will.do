//
//  OverviewViewController.swift
//  Will.Do
//
//  Created by Chase Lau on 1/5/18.
//  Copyright © 2018 Chase Lau. All rights reserved.
//`

import UIKit
import os.log
import UserNotifications
import Chameleon
import EventKit
import DateHelper

class OverviewViewController: UITableViewController {

    // MARK: Properties

    // MARK: Outlets

    // MARK: Actions
    @IBAction func checkBoxTapped(_ sender: UIButton) {
        // Get the UITableViewCell that was tapped
        guard var cell = sender.superview?.superview as? TaskTableViewCell else {
            fatalError("Expected a superview of UITableViewCell, found \(sender.superview?.superview as Optional)")
        }

        // Get the index
        guard let index = self.tableView.indexPath(for: cell) else {
            fatalError("Cell is not visable in the TableView")
        }

        var task: EKReminder
        // Get the corresponding Task
        task = (TaskManager.shared.getTask(inList: index.section, at: index.row))!

        // Update task completed state
        task.isCompleted = !task.isCompleted
        TaskManager.shared.updateTask(task)

        // Update cell
        cell = OverviewViewController.updateTaskTableViewCell(cell, from: task)
    }

    @IBAction func filterTapped(_ sender: Any) {
        TaskManager.shared.showCompletedTasks = !TaskManager.shared.showCompletedTasks
        TaskManager.shared.reloadEventStore()
    }

    // MARK: - ViewController Override
    override func viewDidLoad() {
        // Typical calling superclass
        super.viewDidLoad()

        // Add the tables edit button
        navigationItem.leftBarButtonItem = editButtonItem

        NotificationCenter.default.addObserver(self, selector: #selector(reloadTable), name: Notification.Name("RemindersLoaded"), object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        // Reset the color scheme
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.tintColor = UIButton().tintColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        // Ensure no row is selected
        self.tableView.selectRow(at: nil, animated: true, scrollPosition: .none)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table Properties

    // How many sections the table is divided into
    override func numberOfSections(in tableView: UITableView) -> Int {
        return TaskManager.shared.count()
    }

    // How many rows are in each section
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section >= TaskManager.shared.reminders.count {
            return 0
        } else {
            return TaskManager.shared.reminders[section].count
        }
    }

    // How we can edit (delete/insert)
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            TaskManager.shared.removeTask(fromList: indexPath.section, at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }

    // Table section headers
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {

        guard let header = view as? UITableViewHeaderFooterView else {
            fatalError("View of unknown type: \(view.debugDescription)")
        }

        guard let list = TaskManager.shared.getTaskList(at: section) else {
            fatalError("Unable to find tasklist at index \(section)")
        }

        header.textLabel?.text = list.title

        let listColor = UIColor(cgColor: list.cgColor)
        view.tintColor = listColor
        let headerView = view as! UITableViewHeaderFooterView
        headerView.textLabel?.textColor = ContrastColorOf(listColor, returnFlat: false)
    }

    // MARK: Cell Logic

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "TaskTableViewCell"

        // Attempts to queue up a TaskTableViewCell to use and will throw an error if the cell is not of type TaskTableViewCell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TaskTableViewCell
            else {
                fatalError("The dequeued cell is not an instance of TaskTableViewCell.")
        }

        // Fetches the appropriate task
        if let task = TaskManager.shared.getTask(inList: indexPath.section, at: indexPath.row) {
            let updatedCell = OverviewViewController.updateTaskTableViewCell(cell, from: task)
            return updatedCell
        } else {
            fatalError("Unable to find task at (\(indexPath.section), \(indexPath.row))")
        }
        return UITableViewCell(style: .default, reuseIdentifier: nil)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Call to super class for good measure
        super.prepare(for: segue, sender: sender)

        // Do work depending on type of transition
        switch segue.identifier ?? "" {
        case "AddItem":
            os_log("Adding a new item", log: OSLog.default, type: .debug)
        case "EditItem":
            guard let taskDetailViewController = segue.destination as? TaskViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }

            guard let selectedTaskCell = sender as? TaskTableViewCell else {
                fatalError("Unexpected sender: \(sender as Optional)")
            }

            guard let indexPath = tableView.indexPath(for: selectedTaskCell) else {
                fatalError("The Selected cell is not being displayed by the table")
            }

            var selectedTask: EKReminder! = nil

            selectedTask = TaskManager.shared.getTask(inList: indexPath.section, at: indexPath.row)

            taskDetailViewController.task = selectedTask

        case "ShowSettings":
            os_log("Showing SettingsTableViewController", log: .default, type: .debug)
        default:
            fatalError("Unexpected Segue Identifier: \(segue.identifier as Optional)")
        }

    }

    // MARK: - Public Functions

    /// Populates a TaskTableViewCell with information from task
    ///
    /// - Parameters:
    ///   - cell: The existing cell you want to update
    ///   - task: The task with the information that will be used to update the cell
    /// - Returns: An update version of the cell passed in
    static func updateTaskTableViewCell(_ cell: TaskTableViewCell, from task: EKReminder) -> TaskTableViewCell {

        let updatedCell = cell

        // Set label strings
        updatedCell.nameLabel.text = task.title
        updatedCell.dueDateLabel.text = task.dueDateComponents?.date?.toString(style: .short) ?? "Someday"

        // Checkbox state
        updatedCell.completedCheckbox.isSelected = task.isCompleted

        // Update colors
        if task.isCompleted {
            updatedCell.nameLabel.textColor = .lightGray
            updatedCell.dueDateLabel.textColor = .lightGray
        } else {
            if task.isOverdue {
                updatedCell.dueDateLabel.textColor = .red
            } else {
                updatedCell.dueDateLabel.textColor = .black
            }
            updatedCell.nameLabel.textColor = .black
        }

        return updatedCell
    }

    // MARK: Private Functions

    @objc func reloadTable() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

