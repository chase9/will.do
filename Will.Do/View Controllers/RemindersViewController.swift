//
//  RemindersViewController.swift
//  Will.Do
//
//  Created by Chase Lau on 6/25/18.
//  Copyright © 2018 Chase Lau. All rights reserved.
//

import UIKit
import EventKit

class RemindersViewController: UIViewController {

    // MARK: - Outlets

    @IBOutlet weak var settingsButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        TaskManager.shared.eventStore.requestAccess(to: .reminder) { (granted, error) in
            if granted {
                TaskManager.shared.reloadEventStore()
                self.dismiss(animated: true, completion: nil)
            }
        }
        settingsButton.layer.cornerRadius = 5
    }

    // MARK: - Actions
    @IBAction func goToSettings(_ sender: Any) {
        if let WillDoURL = URL(string: UIApplication.openSettingsURLString) {
            if UIApplication.shared.canOpenURL(WillDoURL) {
                UIApplication.shared.open(WillDoURL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
        }
    }

    // MARK: - ViewController
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
