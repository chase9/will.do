//
//  SettingsTableViewController.swift
//  Will.Do
//
//  Created by Chase Lau on 1/26/18.
//  Copyright © 2018 Chase Lau. All rights reserved.
//

import UIKit
import os.log
import Chameleon

class SettingsTableViewController: UITableViewController {

    // MARK: Outlets
    @IBOutlet weak var versionLabel: UILabel!

    // MARK: Actions
    @IBAction func darkModeChanged(_ sender: UISwitch) {
        UserDefaults.standard.set(sender.isOn, forKey: "darkMode")
    }

    // MARK: - ViewController Overrides
    override func viewDidLoad() {
        super.viewDidLoad()

        //Enable edit button
        self.navigationItem.leftBarButtonItem = editButtonItem

        // Update versionLabel
        let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") ?? "0"
        let build = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") ?? "0"
        versionLabel.text = "Will.Do: \(version)(\(build))"
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTable), name: .EKEventStoreChanged, object: TaskManager.shared.eventStore)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TaskManager.shared.count()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TaskListCell", for: indexPath) as? TaskListTableViewCell else {
            fatalError("Dequeued cell is not of type TaskListTableViewCell")
        }

        guard let list = TaskManager.shared.getTaskList(at: indexPath.row) else {
            fatalError("Unable to get list at index \(indexPath.row)")
        }
        cell.titleLabel.text = list.title
        let listColor = UIColor(cgColor: list.cgColor)
        cell.contentView.superview?.backgroundColor = listColor
        let contractColor = ContrastColorOf(listColor, returnFlat: false)
        cell.titleLabel.textColor = contractColor
        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        if TaskManager.shared.count() == 1 {
            return false
        }
        return true
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            guard let list = TaskManager.shared.getTaskList(at: indexPath.row) else {
                fatalError("Unable to find list at index \(indexPath.row)")
            }
            // Alert user
            let alert = UIAlertController(title: "Are you sure you want to delete \(list.title)?", message: "Deleting this list will remove all tasks associated with it. You cannot undo this.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { action in
                // Delete list if user selects 'Delete'
                TaskManager.shared.removeTaskList(at: indexPath.row)
            }))
            self.present(alert, animated: true, completion: nil)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    /*
    //FIXME: Moving
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        TaskManager.shared.moveTaskList(from: fromIndexPath.row, to: to.row)
        NotificationCenter.default.post(name: NSNotification.Name("reloadOverview"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name("reloadToday"), object: nil)
    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // Hide the right done button while editing
    override func setEditing(_ editing: Bool, animated: Bool) {
        if editing {
            super.setEditing(true, animated: true)
        } else {
            super.setEditing(false, animated: true)
        }
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        switch segue.identifier ?? "" {
        case "AddTaskList":
            os_log("Adding TaskList", log: .default, type: .debug)
        case "EditTaskList":
            os_log("Editing TaskList", log: .default, type: .debug)
            guard let destination = segue.destination as? TaskListViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let selectedCell = sender as? TaskListTableViewCell else {
                fatalError("Unexpected sender: \(sender as Optional)")
            }
            guard let selectedIndex = tableView.indexPath(for: selectedCell) else {
                fatalError("Selected cell is out of view")
            }
            let listToEdit = TaskManager.shared.getTaskList(at: selectedIndex.row)
            destination.taskList = listToEdit
        default:
            fatalError("Unknown identifier for settings")
        }
    }
    
    // MARK: - Private Functions
    @objc private func reloadTable() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

