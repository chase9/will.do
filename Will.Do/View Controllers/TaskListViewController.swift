//
//  TaskListViewController.swift
//  Will.Do
//
//  Created by Chase Lau on 1/26/18.
//  Copyright © 2018 Chase Lau. All rights reserved.
//

import UIKit
import ChromaColorPicker
import Chameleon
import os.log
import EventKit

class TaskListViewController: UIViewController, UITextFieldDelegate, ChromaColorPickerDelegate {

    // MARK: Properties

    var colorPicker: ChromaColorPicker! = nil

    // TaskList that's passed in to edit
    var taskList: EKCalendar?
    var listIndex: Int {
        if let taskList = taskList {
            return TaskManager.shared.getTaskListIndex(for: taskList)
        } else {
            return TaskManager.shared.count()
        }
    }

    // MARK: Outlets

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var colorView: UIView!

    // MARK: Actions

    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismissViewController()
    }

    @IBAction func save(_ sender: UIBarButtonItem) {
        // If the index doesn't match an existing list, assume new list. Else edit list.
        if listIndex == TaskManager.shared.count() {
            taskList = EKCalendar(for: .reminder, eventStore: TaskManager.shared.eventStore)
            taskList?.source = TaskManager.shared.calendars[0].source
        }
        taskList!.title = titleTextField.text ?? "New List"
        taskList!.cgColor = colorView.backgroundColor?.cgColor ?? UIColor(hexString: "FFFFFF").cgColor
        // Force unwrap because save is disabled until list is created
        TaskManager.shared.addTaskList(taskList!)
        // Return to previous view
        dismissViewController()
    }

    // MARK: - ViewController Overrides
    override func viewDidLoad() {
        super.viewDidLoad()

        // Get frame for color picker
        let x = view.frame.height / 2
        var colorPicker: ChromaColorPicker! = nil

        // If there's a tab bar, account for it's height
        if let tabBarHeight = tabBarController?.tabBar.frame.height {
            colorPicker = ChromaColorPicker(frame: CGRect(x: view.frame.width / 2 - (x / 2), y: view.frame.height - x - tabBarHeight, width: x, height: x))
        } else {
            colorPicker = ChromaColorPicker(frame: CGRect(x: view.frame.width / 2 - (x / 2), y: view.frame.height - x, width: x, height: x))
        }

        colorPicker.stroke = 5.0
        colorPicker.supportsShadesOfGray = true

        colorPicker.addTarget(self, action: #selector(colorPickerDidChooseColor(_: color:)), for: UIControl.Event.valueChanged)

        view.addSubview(colorPicker)


        // If a taskList was passed in...
        if let taskList = taskList {
            var color = UIColor(cgColor: taskList.cgColor)
            // Go to and from hex to ensure conversion to UIColor
            color = UIColor(hexString: color.hexCode)
            // Set the color view
            colorPicker.adjustToColor(color)
            // Set the titles
            self.navigationItem.title = taskList.title
            titleTextField.text = taskList.title
        } else {
            // Setup for creating a taskList
            colorPicker.adjustToColor(RandomFlatColorWithShade(.light))
            self.navigationItem.title = "New Task List"
        }
        titleTextField.delegate = self

        colorView.backgroundColor = colorPicker.currentColor
        colorView.layer.borderWidth = 2
        colorView.layer.cornerRadius = 10
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.taskList?.title = titleTextField.text ?? "New List"
        return true
    }

    // MARK: - Functions
    @objc func colorPickerDidChooseColor(_ colorPicker: ChromaColorPicker, color: UIColor) {
        colorView.backgroundColor = colorPicker.currentColor
    }

    // MARK: - Navigation
    private func dismissViewController() {
        let isPresentingInAddTaskListMode = self.presentingViewController is UITabBarController
        if isPresentingInAddTaskListMode {
            self.dismiss(animated: true, completion: nil)
        } else {
            if let owningNavigationController = self.navigationController {
                owningNavigationController.popViewController(animated: true)
            } else {
                fatalError("Not within a NavigationController")
            }
        }
    }
}
