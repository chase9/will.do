//
//  TaskListsTableViewController.swift
//  Will.Do
//
//  Created by Chase Lau on 1/24/18.
//  Copyright © 2018 Chase Lau. All rights reserved.
//

import UIKit
import os.log
import Chameleon
import EventKit

class TaskListsTableViewController: UITableViewController {

    //MARK: Properties
    //This taskList is passed from TaskViewController if we're modifying a task
    var taskList: EKCalendar?

    //MARK: View Overrides
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: TableView Properties
    override func numberOfSections(in tableView: UITableView) -> Int {
        // Should only be one section
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // One row for each taskList
        return TaskManager.shared.count()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TaskListCell", for: indexPath) as? TaskListTableViewCell else {
            fatalError("Dequeued cell is not of type TaskListTableViewCell")
        }
        guard let taskList = TaskManager.shared.getTaskList(at: indexPath.row) else {
            fatalError("Unable to find list at index \(indexPath.row)")
        }
        // Configure the cell...
        // If the user is editing a task, show which list contains that task
        if let owningTaskList = self.taskList {
            // Check if it's for sure a match...
            if indexPath.row == TaskManager.shared.getTaskListIndex(for: owningTaskList) {
                cell.accessoryType = .checkmark
            }
        }
        cell.titleLabel.text = taskList.title
        let listColor = UIColor(cgColor: taskList.cgColor)
        cell.contentView.superview?.backgroundColor = listColor
        let contractColor = ContrastColorOf(listColor, returnFlat: false)
        cell.titleLabel.textColor = contractColor
        cell.tintColor = contractColor
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selectedIndex = tableView.indexPathForSelectedRow {
            self.taskList = TaskManager.shared.getTaskList(at: selectedIndex.row)

            // Update TaskView background color to new TaskList
            let color = UIColor(cgColor: taskList!.cgColor!)
            navigationController?.navigationBar.barTintColor = color
            let textcolor = ContrastColorOf(color, returnFlat: false)
            navigationController?.navigationBar.tintColor = textcolor
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: textcolor]
        }
    }
}

