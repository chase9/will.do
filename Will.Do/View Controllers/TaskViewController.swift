//
//  TaskViewController.swift
//  Will.Do
//
//  Created by Chase Lau on 1/1/18.
//  Copyright © 2018 Chase Lau. All rights reserved.
//

import UIKit
import os.log
import Chameleon
import EventKit

class TaskViewController: UIViewController, UITextFieldDelegate, UINavigationControllerDelegate {

    //MARK: Properties
    // task is either passed by OverviewViewController/TaskList in `prepare(for:sender:)` or constructed as part of adding a new task.
    var task: EKReminder?
    var dueDate = Date()
    var completed = false
    var taskList = TaskManager.shared.getTaskList(at: 0)!

    //MARK: Outlets
    @IBOutlet weak var titleLabel: UITextField!
    @IBOutlet weak var dueDateField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var checkbox: UIButton!
    @IBOutlet weak var taskListButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    var datePicker: UIDatePicker!

    //MARK: Actions
    @IBAction func dueDateDidBeginEditing(_ sender: UITextField) {
        // Make the input View
        let datePickerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 240))

        // Make the actual date picker
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: 40, width: datePickerView.frame.width, height: datePickerView.frame.height - 40))
        datePicker.datePickerMode = .dateAndTime

        // Set datePicker date if there is a duedate already set, 15 minutes ahead of now otherwise
        if let task = task, let date = task.dueDateComponents?.date {
            datePicker.setDate(date, animated: false)
        } else {
            datePicker.setDate(Date().addingTimeInterval(TimeInterval(900)), animated: false)
        }
        datePicker.addTarget(self, action: #selector(self.handleDatePicker), for: UIControl.Event.valueChanged)
        datePickerView.addSubview(datePicker)

        // Create the DatePicker Toolbar
        let toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.sizeToFit()

        // Create the done button
        let nowButton = UIBarButtonItem(title: "Now", style: .plain, target: self, action: #selector(self.setDatePickerToNow))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.resignKeyboard))

        // Add buttons to toolbar
        toolbar.setItems([nowButton, spaceButton, doneButton], animated: true)

        sender.inputView = datePickerView
        datePickerView.addSubview(toolbar)
    }

    @IBAction func checkboxTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }

    // MARK: - ViewController Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        self.saveButton.isEnabled = false
        self.titleLabel.delegate = self
        // Setup the view if a task was passed in
        if let task = task {
            self.saveButton.isEnabled = true
            navigationItem.title = task.title
            titleLabel.text = task.title
            dueDate = task.dueDateComponents?.date ?? Date()
            descriptionTextView.text = task.notes ?? ""
            completed = task.isCompleted
            checkbox.isSelected = completed
            // Force unwrap because we know this task came from the Overview
            taskList = TaskManager.shared.getTaskListForTask(task)!
        }
        updateDateLabel()
        updateTaskListButton()

        // Fix UITextView
        descriptionTextView.textContainerInset = .zero
        descriptionTextView.textContainer.lineFragmentPadding = 0

        // A S T H E T I C
        let listColor = UIColor(cgColor: taskList.cgColor)
        navigationController?.navigationBar.barTintColor = listColor
        let textcolor = ContrastColorOf(listColor, returnFlat: false)
        navigationController?.navigationBar.tintColor = textcolor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: textcolor]
    }

    override func viewDidAppear(_ animated: Bool) {
        titleLabel.becomeFirstResponder()
    }

    /// Thus function is used to check if the textfield should change it's characters. Since it's called every type, I use it to check if we should enable the save button.
    ///
    /// - Parameters:
    ///   - textField: The TextField to check
    ///   - range: The range of charactors to check
    ///   - string: The string to replace with
    /// - Returns: Bool indicating whether the field should change
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = textField.text!

        if text.isEmpty {
            saveButton.isEnabled = false
        } else {
            saveButton.isEnabled = true
        }

        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    //MARK: - Navigation
    private func dismissSelf() {
        // There are different ways to dismiss the viewController depending on how it's shown
        let isPresentingInAddTaskMode = presentingViewController is UITabBarController
        if isPresentingInAddTaskMode {
            dismiss(animated: true, completion: nil)
        } else {
            if let owningNavigationController = navigationController {
                owningNavigationController.popViewController(animated: true)
            } else {
                fatalError("The ViewController is not within a NavigationController")
            }
        }
    }

    @IBAction func cancel(_ sender: UIBarButtonItem) {
        if let task = task {
            task.reset()
        }
        self.dismissSelf();
    }

    @IBAction func save(_ sender: UIBarButtonItem) {
        // Generate the task to be saved
        let title = titleLabel.text
        let note = descriptionTextView.text ?? ""
        completed = checkbox.isSelected
        // Edit task if exists, else add task
        if task == nil {
            task = EKReminder(eventStore: TaskManager.shared.eventStore)
        }
        task?.title = title!
        task?.dueDateComponents = Calendar.current.dateComponents(Set<Calendar.Component>([.minute, .hour, .year, .month, .day]), from: dueDate)
        task?.notes = note
        task?.isCompleted = completed
        task?.calendar = taskList
        
        let alarm = EKAlarm(absoluteDate: dueDate)
        task?.addAlarm(alarm)
        
        TaskManager.shared.updateTask(task!)
        self.dismissSelf()
    }

    // Prepare will let you prepare a view controller before it's navigated away from
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Call to superclass is good practice
        super.prepare(for: segue, sender: sender)
        switch segue.identifier ?? "" {
            // If we're transitioning to the taskList picker, pass current taskList
        case "SelectTaskList":
            if let destination = segue.destination as? TaskListsTableViewController {
                destination.taskList = taskList
            }
            // Otherwise prepare to unwind to Overview
        default:
            os_log("Unknown segue identifier", log: .default, type: .debug)
        }
    }

    @IBAction func unwindToTaskView(sender: UIStoryboardSegue) {
        // Check if we came from the task list
        if let sourceViewController = sender.source as? TaskListsTableViewController, let taskList = sourceViewController.taskList {
            self.taskList = taskList
            updateTaskListButton()
        } else {
            fatalError("Unexpected source: \(sender.source)")
        }
    }

    //MARK: - Private Functions

    @objc private func setDatePickerToNow() {
        datePicker.setDate(Date(), animated: true)
        handleDatePicker(sender: datePicker)
    }

    @objc private func handleDatePicker(sender: UIDatePicker) {
        dueDate = sender.date
        updateDateLabel()
    }

    @objc private func resignKeyboard() {
        self.view.endEditing(true)
    }

    private func updateDateLabel() {
        // Format Date
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        dateFormatter.dateStyle = .long
        dateFormatter.locale = Locale.current
        dueDateField.text = dateFormatter.string(from: dueDate)
        if Date() > dueDate {
            dueDateField.textColor = .red
        } else {
            dueDateField.textColor = .black
        }
    }

    private func updateTaskListButton() {
        taskListButton.setTitle(taskList.title, for: UIControl.State.normal)
    }
}

