//
//  TodayTableViewController.swift
//  Will.Do
//
//  Created by Chase Lau on 2/4/18.
//  Copyright © 2018 Chase Lau. All rights reserved.
//

import UIKit
import os.log
import EventKit

class TodayTableViewController: UITableViewController {

    // MARK: Properties
    var relativeList: [EKReminder]!

    // MARK: Outlets


    // MARK: Actions
    @IBAction func checkBoxTapped(_ sender: UIButton) {
        // Get the UITableViewCell that was tapped
        guard let cell = sender.superview?.superview as? TaskTableViewCell else {
            fatalError("Expected a superview of UITableViewCell, found \(sender.superview?.superview as Optional)")
        }

        // Get the index
        guard let index = self.tableView.indexPath(for: cell) else {
            fatalError("Cell is not visable in the TableView")
        }

        let task = relativeList![index.row]

        // Update task completed state

        task.isCompleted = !task.isCompleted
        TaskManager.shared.updateTask(task)

        cell.completedCheckbox.isSelected = task.isCompleted
    }

    // MARK: - ViewController Overrides

    override func viewDidLoad() {
        super.viewDidLoad()

        self.relativeList = [EKReminder]()

        reloadTable()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTable), name: .EKEventStoreChanged, object: TaskManager.shared.eventStore)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return relativeList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "TaskTableViewCell"

        // Attempts to queue up a TaskTableViewCell to use and will throw an error if the cell is not of type TaskTableViewCell
        guard var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? TaskTableViewCell
            else {
                fatalError("The dequeued cell is not an instance of TaskTableViewCell.")
        }

        let task = relativeList![indexPath.item]

        cell = OverviewViewController.updateTaskTableViewCell(cell, from: task)

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - Private Functions
    @objc private func reloadTable() {
        // Fill relative list with uncompleted tasks, sort by dueDate
        let predicate = TaskManager.shared.eventStore.predicateForIncompleteReminders(withDueDateStarting: nil, ending: nil, calendars: nil)
        TaskManager.shared.eventStore.fetchReminders(matching: predicate) { (reminders) in
            self.relativeList = reminders
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
}
